//
//  main.cpp
//  Lab4
//
//  Created by Wojciech Beker on 21.03.2016.
//  Copyright © 2016 Wojciech Beker. All rights reserved.
//

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <sys/time.h>
#include <cmath>

using namespace std;

const int ROZMIAR = 1000000;
int tablica[ROZMIAR];
int tablica2[ROZMIAR];
int pomocnicza[ROZMIAR];


//Funkcja sprawdzajaca czy tablica zostala poprawnie posortowana
int sprawdz(int tab[]) {
    int x = tab[0];
    int kontrolka = 0;
    for (int i = 0; i < ROZMIAR; i++) {
        if (tab[i+1] >= x) {
            x = tab[i+1];
            kontrolka = 1;
        }
        else {
            cout << "Zle posortowana tablica\n";
            kontrolka = 0;
            break;
        }
    }
    return kontrolka;
}


//Funkcja losujaca i zapisujaca do tablicy
void losuj() {
    for(int i = 0; i < ROZMIAR; i++) {
        tablica[i] = (rand() % (10001));
    }
}


//Funkcja odwracajaca tablice
void odwroc() {
    int temp;
    for (int i = 0; i < ROZMIAR/2; i++) {
        temp = tablica[ROZMIAR-i-1];
        tablica[ROZMIAR-i-1] = tablica[i];
        tablica[i] = temp;
    }
}


//Implementacja sortowania quicksort
void quicksort( int tab[], int lewy, int prawy)
{
    int i = lewy;
    int j = prawy;
    int x = tab[( lewy + prawy ) / 2 ];
    do
    {
        while( tab[ i ] < x )
            i++;
        
        while( tab[ j ] > x )
            j--;
        
        if( i <= j )
        {
            swap( tab[ i ], tab[ j ] );
            
            i++;
            j--;
        }
    } while( i <= j );
    if( lewy < j ) quicksort( tab, lewy, j);
    if( prawy > i ) quicksort( tab, i, prawy);
    
}


//Dodatkowa funkcja do sortowania przez scalanie (scala podtablice)
void scal(int a[], int lewy, int srodek, int prawy)
{
    int i, j;
    
    //lewy do pomocniczej
    for(i = srodek + 1; i>lewy; i--)
        pomocnicza[i-1] = a[i-1];
    
    //prawy do pomocniczej
    for(j = srodek; j<prawy; j++)
        pomocnicza[prawy+srodek-j] = a[j+1];
    
    //scalenie
    for(int k=lewy;k<=prawy;k++)
        if(pomocnicza[j]<pomocnicza[i])
            a[k] = pomocnicza[j--];
        else
            a[k] = pomocnicza[i++];
}


//Implementacja sortowania przez scalanie
void scalanie(int tab[],int lewy, int prawy)
{
    //jesli jeden to posortowany
    if(prawy<=lewy) return;
    
    //srodek
    int srodek = (prawy+lewy)/2;
    
    //podziel na lewy i prawy
    scalanie(tab, lewy, srodek);
    scalanie(tab, srodek+1, prawy);
    
    scal(tab, lewy, srodek, prawy);
}


//Implementacja algorytmu Shella przy pomocy template
template <class type>
type Shell(type tab[], int ile) {
    int h = 0;
    int i = 0;
    int x = 0;
    
    for(h = 1; h < ile; h = 3 * h + 1);
    h /= 9;
    
    while(h)
    {
        for(int j = ile - h - 1; j >= 0; j--)
        {
            x = tab[j];
            i = j + h;
            while((i < ile) && (x > tab[i]))
            {
                tab[i - h] = tab[i];
                i += h;
            }
            tab[i - h] = x;
        }
        h /= 3;
    }
    
   
    return 0;
}


//Funkcja kopiujaca wartosci z tablicy 1 do tablicy 2 (aby wszystkie algorytmy pracowaly na tych samych danych)
void kopiuj() {
    for (int i = 0; i < ROZMIAR; i++) {
        tablica2[i] = tablica[i];
    }
}



int main(int argc, const char * argv[]) {
    srand( time(NULL));
    timeval t1, t2;
    double elapsedTime;
    double pomiary[3000];
    //double wartosci[7] = {0, 0.25, 0.5, 0.75, 0.95, 0.99, 0.997};
    losuj();
    kopiuj();
    double wynik = 0.0;
    for (int j = 10000; j <= 1000000; j*=10) {
        for (int i = 0; i < 100; i++) {
            //for (int z = 0; z<7; z++) {
            losuj();
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+800] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1600] = elapsedTime;
            
            quicksort(tablica, 0, 0.25*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+100] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+900] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1700] = elapsedTime;
            
            quicksort(tablica, 0, 0.50*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+200] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1000] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1800] = elapsedTime;
            
            quicksort(tablica, 0, 0.75*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+300] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1100] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1900] = elapsedTime;
            
            quicksort(tablica, 0, 0.95*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+400] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1200] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2000] = elapsedTime;
            
            quicksort(tablica, 0, 0.99*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+500] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1300] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2100] = elapsedTime;
            
            quicksort(tablica, 0, 0.997*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+600] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1400] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2200] = elapsedTime;
            
            quicksort(tablica, 0, ROZMIAR);
            odwroc();
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+700] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1500] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2300] = elapsedTime;

            
        }
        cout << "Dla " << j << " danych:\n";
        cout << "Sortowanie Shella: ";
        for (int i = 0; i<800; i++) {
            wynik += pomiary[i];
            if (i == 99 || i==199 || i==299 || i==399 || i==499 || i==599 || i==699 || i==799) {
                wynik /= 100;
                cout << wynik << "  ";
                wynik = 0;
            }
        }
        
        cout << endl;
        
        cout << "Sortowanie przez scalanie: ";
        for (int i = 1600; i<2400; i++) {
           // cout << "TEST";
            wynik += pomiary[i];
            if (i == 2199 || i==2299 || i==1699 || i==1799 || i==1899 || i==1999 || i==2099 || i==2399) {
                wynik /= 100;
                cout << wynik << "  ";
                wynik = 0;
            }
        }

        cout << endl;
        
        cout << "Sortowanie szybkie: ";
        for (int i = 800; i<1600; i++) {
            wynik += pomiary[i];
            if (i == 1499 || i==999 || i==1099 || i==1199 || i==1299 || i==1399 || i==1599 || i==899) {
                wynik /= 100;
                cout << wynik << "  ";
                wynik = 0;
            }
        }

        cout << endl;

    }

    for (int j = 50000; j <= 500000; j*=10) {
        for (int i = 0; i < 100; i++) {
            losuj();
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+800] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1600] = elapsedTime;
            
            quicksort(tablica, 0, 0.25*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+100] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+900] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1700] = elapsedTime;
            
            quicksort(tablica, 0, 0.50*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+200] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1000] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1800] = elapsedTime;
            
            quicksort(tablica, 0, 0.75*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+300] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1100] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1900] = elapsedTime;
            
            quicksort(tablica, 0, 0.95*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+400] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1200] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2000] = elapsedTime;
            
            quicksort(tablica, 0, 0.99*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+500] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1300] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2100] = elapsedTime;
            
            quicksort(tablica, 0, 0.997*j);
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+600] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1400] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2200] = elapsedTime;
            
            quicksort(tablica, 0, ROZMIAR);
            odwroc();
            
            kopiuj();
            gettimeofday(&t1, NULL);
            Shell(tablica2,j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+700] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            quicksort(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+1500] = elapsedTime;
            
            kopiuj();
            gettimeofday(&t1, NULL);
            scalanie(tablica2, 0, j);
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;
            pomiary[i+2300] = elapsedTime;
            
        }
        
        cout << "Dla " << j << " danych:\n";
        cout << "Sortowanie Shella: ";
        for (int i = 0; i<800; i++) {
            wynik += pomiary[i];
            if (i == 99 || i==199 || i==299 || i==399 || i==499 || i==599 || i==699 || i==799) {
                wynik /= 100;
                cout << wynik << "  ";
                wynik = 0;
            }
        }
        
        cout << endl;
        
        cout << "Sortowanie przez scalanie: ";
        for (int i = 1600; i<2400; i++) {
            // cout << "TEST";
            wynik += pomiary[i];
            if (i == 2199 || i==2299 || i==1699 || i==1799 || i==1899 || i==1999 || i==2099 || i==2399) {
                wynik /= 100;
                cout << wynik << "  ";
                wynik = 0;
            }
        }
        
        cout << endl;
        
        cout << "Sortowanie szybkie: ";
        for (int i = 800; i<1600; i++) {
            wynik += pomiary[i];
            if (i == 1499 || i==999 || i==1099 || i==1199 || i==1299 || i==1399 || i==1599 || i==899) {
                wynik /= 100;
                cout << wynik << "  ";
                wynik = 0;
            }
        }
        
        cout << endl;
    }

    return 0;
}

